sshpass -p $1 ssh -o StrictHostKeyChecking=no root@157.230.73.44 <<-'ENDSSH' 
    docker login -u tklusz -p $2 registry.gitlab.com 
	docker stop $(docker ps -a -q)
    docker rm $(docker ps -a -q)
	docker rmi $(docker images -a -q)
	docker pull registry.gitlab.com/tklusz/digitaloceanwebsite
    docker run --name digitaloceanwebsite -p 80:80 -d registry.gitlab.com/tklusz/digitaloceanwebsite
ENDSSH
